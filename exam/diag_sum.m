function s=diag_sum(x)
x=input('x=');
[m,n]=size(x);
h=min(m,n);
i=(h+1)/2;
y=x';
if mod(h,2)==0
s=sum(diag(x))+sum(diag(y));
else
s=sum(diag(x))+sum(diag(y))-x(i,i);
end
end

